/*
import java.util.*

    class Burger(val name: String)

    class Marco : Observable() {

        val name = "Marco"

        fun cookBurger(name : String){
            var burger = Burger(name)

            //Call setChanges() prior to calling notifyObservers()
            setChanged() //Inherited from Observable()
            notifyObservers(burger) //Inherited from Observable()
        }
    }

    class Anabel : Observer{

        val name = "Anabel"

        override fun update(o: Observable?, arg: Any?) {
            when (o){
                is Bob -> {
                    if (arg is Burger){
                        println("$name esta sirviendo ${arg.name} cocinado por ${o.name}")
                    }
                }
                else -> println(o?.javaClass.toString())
            }
        }
    }

    fun main(args : Array<String>){
        val bob = Bob()
        bob.addObserver(Tina())

        bob.cookBurger("Hace falta una buena comida para saber la hamburguesa")
    }
*/